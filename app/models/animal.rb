class Animal < ApplicationRecord
    belongs_to :territory
    has_many: animal_food
    has_many: foods, through: :animal_foods
    end
