class Territory < ApplicationRecord
    validates :name, presence:true
    validates :climate, presence:true
    validate :minimum_size
    has_many :animals
    
    private

    def minimum_size
        if size < 100 
            errors.add(:size, "too small")
        end
    end

end
