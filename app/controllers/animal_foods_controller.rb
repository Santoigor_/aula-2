class AnimalFoodsController < ApplicationController
  before_action :set_animal_food, only: [:show, :update, :destroy]

  # GET /animal_foods
  def index
    @animal_foods = AnimalFood.all

    render json: @animal_foods
  end

  # GET /animal_foods/1
  def show
    render json: @animal_food
  end

  # POST /animal_foods
  def create
    @animal_food = AnimalFood.new(animal_food_params)

    if @animal_food.save
      render json: @animal_food, status: :created, location: @animal_food
    else
      render json: @animal_food.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /animal_foods/1
  def update
    if @animal_food.update(animal_food_params)
      render json: @animal_food
    else
      render json: @animal_food.errors, status: :unprocessable_entity
    end
  end

  # DELETE /animal_foods/1
  def destroy
    @animal_food.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_animal_food
      @animal_food = AnimalFood.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def animal_food_params
      params.require(:animal_food).permit(:animal_id, :food_id)
    end
end
