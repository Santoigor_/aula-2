Rails.application.routes.draw do
  resources :animal_foods
  resources :foods
  post '/territories/:territory_id/animals', to: 'animals#create'
  get '/territories/:id/animals', to: 'territories#animals'
  
  resources :territories
  resources :animals

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/', to: 'static_pages#home'
  
end
