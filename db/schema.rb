# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_23_164947) do

  create_table "animal_foods", force: :cascade do |t|
    t.integer "animal_id", null: false
    t.integer "food_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["animal_id"], name: "index_animal_foods_on_animal_id"
    t.index ["food_id"], name: "index_animal_foods_on_food_id"
  end

  create_table "animals", force: :cascade do |t|
    t.string "name"
    t.date "birthdate"
    t.integer "type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "territory_id"
    t.index ["territory_id"], name: "index_animals_on_territory_id"
  end

  create_table "foods", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "territories", force: :cascade do |t|
    t.string "name"
    t.integer "size"
    t.string "climate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
  end

  add_foreign_key "animal_foods", "animals"
  add_foreign_key "animal_foods", "foods"
  add_foreign_key "animals", "territories"
end
