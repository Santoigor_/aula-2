class CreateAnimals < ActiveRecord::Migration[6.0]
  def change
    create_table :animals do |t|
      t.string :name
      t.date :birthdate
      t.integer :type

      t.timestamps
    end
  end
end
