class CreateAnimalFoods < ActiveRecord::Migration[6.0]
  def change
    create_table :animal_foods do |t|
      t.references :animal, null: false, foreign_key: true
      t.references :food, null: false, foreign_key: true

      t.timestamps
    end
  end
end
