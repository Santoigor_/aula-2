class AddTerritoryToAnimal < ActiveRecord::Migration[6.0]
  def change
    add_reference :animals, :territory, foreign_key: true
  end
end
