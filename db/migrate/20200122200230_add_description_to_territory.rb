class AddDescriptionToTerritory < ActiveRecord::Migration[6.0]
  def change
    add_column :territories, :description, :string
  end
end
